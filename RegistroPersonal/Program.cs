﻿// Online C# Editor for free
// Write, Edit and Run your C# code using C# Online Compiler

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;


public class Datos {

    string nombres = "";
    string apellidos = "";
    int edad = 0;
    string cedula = "";

    public string getNombres(){
        return this.nombres;
    }

    public string getApellidos(){

        return this.apellidos;
    }

    public int getEdad() {
        return this.edad;
    }

    public string getCedula(){
        return this.cedula;
    }

    public void setNombres (){
        string nombres = "";
        int longitud = 0;
        
        do{
            Console.WriteLine ("Digite los nombres: ");
            nombres = Console.ReadLine();
            if ( nombres.Length >= 3 && verificarSiCadena(nombres)){
                this.nombres = nombres;
                longitud = nombres.Length;
            }
            else
                Console.WriteLine("El nombre no es permitido...");
            
        }while( (longitud < 3 ) );
        
    }

    public void setApellidos (){
        
        string apellidos = "";
        int longitud = 0;
        
        do{
            Console.WriteLine ("Digite los apellidos: ");
            apellidos = Console.ReadLine();
            
            if(apellidos.Length > 3 && verificarSiCadena(apellidos)){
                this.apellidos = apellidos;
                longitud = apellidos.Length;
            }else
                Console.WriteLine("Los apellidos no son permitido...");
            
        }while(longitud < 3);
        
        //this.apellidos  = apellidos.Replace(' ', '\n');
    }

    private bool verificarSiCadena(string cad){

        string patron = @"^[A-Za-z ]*$";
          Regex miRegex = new Regex(patron);
          MatchCollection elMatch = miRegex.Matches(cad);
          if(elMatch.Count > 0)
            return true;
          
          return false;

    }
    
    public void setEdad (){
        
        string edad = "";
        int verificador = 0;
        
        Console.WriteLine ("Digite la edad: ");
        
        do {
            edad = Console.ReadLine();
            
            if( (int.TryParse(edad, out verificador)) == false ){
                Console.WriteLine("Solo se permiten numeros...");
            }else if(verificador < 1){
                Console.WriteLine("Edad no valida...");
            }
            
        }while(verificador == 0);
        
        this.edad  = Convert.ToInt32(edad);
    }

    // Metodo para ingresar cedula por primera vez
    public void setCedula(string cedula){
        this.cedula = cedula;
    }
}

public class HelloWorld
{
    public static void Main(string[] args)
    {
        string Op = "";
        List<Datos> DataBase = new List<Datos>();
        
        while (true){
            
            Console.WriteLine (
                "Opciones de la applicacion\n" + 
                "1 -> Calculadora\n" + 
                "2 -> Registro de Personal\n" + 
                "3 -> Salir\n" +
                "Digite la opcion: ");
                
            Op = Console.ReadLine();
 
            switch( Op ){
                case "1":{
                    Calculadora();
                    break;
                }
                case "2":{
                    RegistroPersonal();
                    break;
                }
                case "3":{
                    sms ("Fin de la aplicacion...\n");
 
                    return ;
                }
                default:{
                    sms ("Opcion incorrecta...\n");
                    break;
                }
              
               
            }
            
            Console.ReadLine(); //Pause
        }


        void Calculadora(){
            
            string Numero1 = "", Numero2 = "";
            string Op = "";
            double validar = 0;

            while (true){

                Console.WriteLine (
                    "Opciones de la calculadora\n" +
                    " 1 -> Suma\n" +
                    " 2 -> Resta\n" +
                    " 3 -> Multiplicación\n" +
                    " 4 -> Divisiónn" +
                    " 5 -> Salir de calculadora\n" +
                    "Digite la opcion: ");
                    
               
                do{
                    Op = Console.ReadLine();
                    if ( !(double.TryParse(Op, out validar)) ){
                        sms("Solo se permiten numeros...");
                        validar = 0;
                    }else if(Convert.ToInt32(Op) >= 1 &&
                    Convert.ToInt32(Op) <= 4){
                        
                        do{
                            sms ("Digite el primero Numero");
                            Numero1 = Console.ReadLine();
                            sms ("Digite el Segundo Numero");
                            Numero2 = Console.ReadLine();
                            
                            if (!(double.TryParse(Numero1, out validar)) || 
                                !(double.TryParse(Numero2, out validar)) ){
                                sms("Solo se permiten numeros");
                                validar = 0;
                            }
                            
                        }while(validar == 0);
                        
                    }
                }while(validar == 0);
            
              
                switch( Op ){
                    
                    case "1":{
                        Console.WriteLine ("{0} mas {1} es igual a: {2}\n",
                          Numero1,
                          Numero2,
                          Convert.ToDouble(Numero1) +
                          Convert.ToDouble(Numero2));

                        break;
                    }
                    case "2":{
                       
                        Console.WriteLine ("{0} - {1} = {2}", Numero1, Numero2, Convert.ToDouble(Numero1) - Convert.ToDouble(Numero2));
                        break;
                    }
                    case "3":{
                     
                        Console.WriteLine ("{0} * {1} = {2}", Numero1, Numero2, Convert.ToDouble(Numero1) * Convert.ToDouble(Numero2));
                        break;
                    }
                    case "4":{
                    
                        Console.WriteLine ("{0} / {1} = {2}", Numero1, Numero2, Convert.ToDouble(Numero1) / Convert.ToDouble(Numero2));
                  
                        break;
                    }
                    case "5":{
                        sms ("Salio de la calculadora...\n");
     
                        return;
                    }
                    default:{
                        sms ("ERROR...");
                        break;
                    }
                }
                Console.ReadLine(); //Pause
            }
        }

        void RegistroPersonal(){
            
            string Op = "";

            while(true){
                
                Console.WriteLine (
                    "***Opciones del registro***\n" +
                    "1 -> Agregar Registros\n" +
                    "2 -> Ver Registros\n" + 
                    "3 -> Buscar un registro\n" +
                    "4 -> Eliminar todos los registros\n" +
                    "5 -> Eliminar un registro especifico\n" +
                    "6 -> Editar un registro\n" +
                    "7 -> Salir de la applicacion\n" +
                    "Digite la opcion: ");

                Op = Console.ReadLine();
                
                switch(Op){

                    // Agregar un registro
                    case "1":{
                        agregarUnRegistro();
                        break;
                    }
                    
                    // Ver todos los registros
                    case "2":{
                        if (DataBase.Count > 0){
                            verRegistros("");                        
                        }else
                            sms("No hay registros");
                       
                        break;
                    }

                    // Busca un registro especifico
                    case "3":{
                        if (DataBase.Count > 0){
                            // Para buscar y ver se usa la misma funcion
                            string cedula = "";

                            Console.WriteLine ("Ingrese la cedula: ");
                            cedula = Console.ReadLine();
                            verRegistros(cedula);
                        }else
                            sms("No hay registros");
        
                        break;
                    }

                    // Elimina todos los registros
                    case "4":{
                        if ( DataBase.Count > 0 ){
                            DataBase.Clear();
                            sms("Los registros fueron eliminados...");
                        }else{
                            sms("No hay registros...");
                        }
                       
                        break;
                    }

                    // Elimina un registro en especifico
                    case "5":{
                        if (DataBase.Count > 0){
                            eliminarUnRegistro();
                        }else
                            sms("No hay registros");
                        break;
                    }
                    
                    //Editar un registro
                    case "6":{
                            
                            if(DataBase.Count > 0){
                                actualizarRegistro();
                            }else
                                sms("No hay registros...");
                        break;
                    }
                    
                    case "7":{
                        sms("Salio de Registro de personas...\n");
                        return;
                    }

                    default:{
                        sms("Opcion incorrecta...");
                        break;
                    }
                }
                sms("\nTeclear Enter...");
                Console.ReadLine();
            }
        }

        // Metodo para ver todos los registros y para ver uno en especifico
        void verRegistros(string cedula){

            int itera = 0;
            int mensaje = 0;
            
            Console.WriteLine("".PadRight(47,'_'));
            
            if(cedula == ""){
                
                foreach (Datos i in DataBase){
                    
                    Console.WriteLine("| No        | {0} |", (itera + 1).ToString().PadRight(32, ' '));
               
                    Console.WriteLine("| Cedula    | {0} |", i.getCedula().PadRight(32, ' '));
          
                    Console.WriteLine("| Nombres   | {0} |", i.getNombres().PadRight(32, ' '));
           
                    Console.WriteLine("| Apellidos | {0} |", i.getApellidos().PadRight(32, ' '));
               
                    Console.WriteLine("| Edad      | {0} |", i.getEdad().ToString().PadRight(32, ' '));
                    
                    Console.WriteLine("".PadRight(47,'_'));
    
                    itera ++;
                }
            }else{
                foreach (Datos i in DataBase){
                    if (i.getCedula() == cedula){
                        Console.WriteLine("| No        | {0} |", (itera + 1).ToString().PadRight(32, ' '));
                   
                        Console.WriteLine("| Cedula    | {0} |", i.getCedula().PadRight(32, ' '));
              
                        Console.WriteLine("| Nombres   | {0} |", i.getNombres().PadRight(32, ' '));
               
                        Console.WriteLine("| Apellidos | {0} |", i.getApellidos().PadRight(32, ' '));
                   
                        Console.WriteLine("| Edad      | {0} |", i.getEdad().ToString().PadRight(32, ' '));
                        
                        Console.WriteLine("".PadRight(47,'_'));
                        
                        mensaje = 1;
                   
                    }else
                    
                    itera ++;
                }
                if (mensaje == 0)
                sms("No se encontro el registro...");
            }
            
            
        
        }
        
        void actualizarRegistro(){
            string cedulaBuscar = "";
            string cedulaNueva = "";
            //int indiceDato = 0;
            int iterador = 0;
            
            sms("Ingrese la cedula del registro a editar:");
            cedulaBuscar = Console.ReadLine();

            IEnumerable<Datos> Query = from registro in DataBase
                where registro.getCedula() == cedulaBuscar
                select registro;
            
            // SI el registro a buscar se encuentra iterador == 1
            foreach (Datos i in  Query)
                iterador ++;

            if( iterador > 0 ){
            
              sms("Ingrese la cedula Nueva: ");
              cedulaNueva = Console.ReadLine();

              if(cedulaBuscar == cedulaNueva){
                // Actualizar los datos en el objeto de la primera consulta
                foreach (Datos i in Query){
                  i.setNombres();
                  i.setApellidos();
                  i.setEdad();
                  i.setCedula(cedulaNueva);
                }
              }
              
              if(cedulaBuscar != cedulaNueva){

                // SI el registro nuevo se encuentra
                IEnumerable<Datos> Query2 = from registro in DataBase
                  where registro.getCedula() == cedulaNueva
                  select registro;
            
                // SI el registro nuevo se encuentra
                iterador = 0;
                foreach (Datos i in  Query2)
                  iterador ++;
            
                if (iterador > 0){
                  sms("No se permiten cedulas repetidas...");
                  return;
                }else{
                    
                  // Actualizar los datos en el objeto de la segunda consulta
                  foreach (Datos i in Query){
                    i.setNombres();
                    i.setApellidos();
                    i.setEdad();
                    i.setCedula(cedulaNueva);
                  }
                }
              }

            
            }else{
                sms("No se encontro el registro...");
                
            }
        }

        void eliminarUnRegistro(){

            string cedula = "";

            sms ("Ingrese la cedula: ");
            cedula = Console.ReadLine();
            int contador = 0;
            
            foreach (Datos i in DataBase){
                if (i.getCedula() == cedula){
                    DataBase.RemoveAt(contador);    
                    sms("Registro eliminado...");
                    return;
                }
                contador ++;
            }
            sms("No se encontro el registro...");
        }

        void agregarUnRegistro(){

            string cantRegistros = "";
            int iterador = 0;
            int control = 0;
                        
            do{
                sms("Cuantos registros quiere ingrear?:");
                cantRegistros = Console.ReadLine();
            
                if( verificarSiNumero(cantRegistros) == true ){
                    iterador = Convert.ToInt32(cantRegistros);
                }else
                    iterador = 0;
                            
            }while( iterador == 0);
                    
            for (int i = 0; i < iterador; i++ ){
                Console.WriteLine("Registro [{0}]: ", i + 1 );

                Datos agregarDato = new Datos();
                string cedula = "";
                
                
                do{

                  sms("Ingrese la cedula: ");
                  cedula = Console.ReadLine();
    
                  if(verificarCedula(cedula) == false){
                    sms("Ingrese solo numeros y letras. [A-Za-z], [0-9]");
                    return;
                  }

                }while(verificarCedula(cedula) == false);
                
                
                /*Si ya hay registros, verificar
                si la cedula no ha sido ya
                ingresada*/
                if ( DataBase.Count > 0 ){
                
                    //******************
                    IEnumerable<Datos> Query = from registro in DataBase
                    where registro.getCedula() == cedula
                    select registro;
                    
                    foreach (Datos ii in Query)
                        control ++;
                    
                    if(control > 0){
                        Console.WriteLine("La cedula ya existe...");
                        return;
                    }else{
                        Datos nuevoDato = new Datos();
                        
                        nuevoDato.setNombres();
                        nuevoDato.setApellidos();
                        nuevoDato.setEdad();
                        nuevoDato.setCedula(cedula);
                    
                         DataBase.Add(nuevoDato);    
                        
                    }
                //***************
                }else{
                    Datos nuevoDato = new Datos();
                        
                    nuevoDato.setNombres();
                    nuevoDato.setApellidos();
                    nuevoDato.setEdad();
                    nuevoDato.setCedula(cedula);
                    
                    DataBase.Add(nuevoDato);   
                }
            }
        }

        
        //Retorna true si es valida
        bool verificarCedula(string cedula){
          string patron = @"^[A-Za-z0-9]*$";
          Regex miRegex = new Regex(patron);
          MatchCollection elMatch = miRegex.Matches(cedula);
          if(elMatch.Count > 0)
            return true;
        
          return false;

        }       
        
        // Funcion para verificar si una cadena ingresada es numero o no
        // Si es devuelve 1 si no devuelve 0
        bool verificarSiNumero(string cantidadRegistros){
            
            int numeroIngresado = 0;
            if( (int.TryParse(cantidadRegistros, out numeroIngresado)) == false ){
                sms("Dato incorrecto...");
                return false;
            }else if (numeroIngresado == 0){
                sms("Dato incorrecto...");
                return false;
            }else
                return true;
        }
   
        
        //Recibe el mensaje
        void sms(string sms){
 
            // Mensaje de la app que llama la funcion
            Console.WriteLine(sms + "\n");
        }

        static bool formatoCedula(string cedula){
        
            /*CONDICIONES DE LA VERIFICACION*/
            // Los 3 primedos digitos deben ser numericos
            // Luego debe ser si o si un guion
            
            // Las siguientes dos cifras deben ser numericos dentro del 1 y el 30
            
            // Las siguientes dos cifras deben ser numericos dentro del 1 y 12
            
            // Las siguientes dos cifras deben de ser numericos
            // de 00 a 99
            
            int iterador = 0;
            string nuevaCadena = "";
            
            //Primeros 3 datos de la cedula
            string codigo1 = "";
            
            //Primer guion de la cedula
            string guion1 = "";
            
            //dia de nacimiento
            string dia = "";
            
            //mes de nacimiento
            string mes = "";
            
            //anio de nacimiento
            string anio = "";
            
            string guion2 = "";
            
            //5 datos del 1 al 9 o de A-Z
            string codigo2 = "";
            
            if (cedula.Length < 16 || cedula.Length > 16){
                Console.WriteLine("Cedula no esta completa...");
            }else{
                
                for (int i = 0; i < cedula.Length; i++){
                    //Las siguientes condiciones son para capturar cada una de las partes de la cedula
                    if (i < 3)
                        codigo1 += cedula[iterador];
                    else if (i == 3)
                        guion1 += cedula[iterador];
                    else if (i > 3 && i < 6)
                        dia += cedula[iterador];
                    else if(i > 5 && i < 8)
                        mes += cedula[iterador];
                    else if(i > 7 && i < 10)
                        anio += cedula[iterador];
                    else if(i == 10)
                        guion2 += cedula[iterador];
                    else if(i > 10 && i < 16)
                        codigo2 += cedula[iterador];
                    iterador++;
                }
            }
            
            // Es esta variable tiene el valor de la salida de Int.TryParse
            int numeroOut = 0;

            if ( (Int32.TryParse(codigo1, out numeroOut)) == true ){
                if( guion1 == "-" ){
                    if( (Int32.TryParse(dia, out numeroOut)) == true ){                    
                        if( numeroOut > 0 && numeroOut < 32 ){
                            if( (Int32.TryParse(mes, out numeroOut)) == true ){
                                if( numeroOut > 0 && numeroOut < 13 ){
                                    if( (Int32.TryParse(anio, out numeroOut)) == true ){
                                        if( numeroOut >= 0 && numeroOut < 100 ){
                                            if( guion2 == "-" ){
                                                string patron = @"\d{4}[A-Z]";
                                                Regex miRegex = new Regex(patron);
                                                MatchCollection elMatch = miRegex.Matches(codigo2);
                                                if(elMatch.Count > 0){
                                                    return true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return false;
        }

    }
}

