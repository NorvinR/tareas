
/*Actualizacion de llaves primarias y secundarias con on "update cascade" (no recomendable)*/

use tutorialw3c;

-- drop table if exists CustomersTest;
-- drop table if exists OrdersTest;

CREATE TABLE CustomersTest(
	CustomerID		VARCHAR(30) NOT NULL,
	State			VARCHAR (10) NOT NULL DEFAULT "ACTIVE",
	PRIMARY			KEY (CustomerID)
);

CREATE TABLE OrdersTest(
	CustomerID	VARCHAR(30) NOT NULL,
    State		VARCHAR(10) NOT NULL DEFAULT "ACTIVE",
    FOREIGN		KEY (CustomerID) REFERENCES CustomersTest(CustomerID) ON UPDATE CASCADE
);

INSERT INTO CustomersTest(CustomerID) values ("NUMERO3");
INSERT INTO OrdersTest(CustomerID) values ("NUMERO3");

update CustomersTest A inner join OrdersTest B on A.CustomerID = B.CustomerID set
	A.CustomerID = "NUMERO1",
    A.State = "ACTIVE",
    -- B.CustomerID = "NUMERO3",
    B.State = "ACTIVE"
where A.CustomerID = "NUMERO2";

select * from CustomersTest;
select * from OrdersTest;
DELETE FROM Orders A where A.CustomerID = "Numero3";

DROP PROCEDURE IF EXISTS UpdateMMV;


/*Function. Se recomiendda que contenga cosas puntuales menos complejas que el
procedimiento*/



DELIMITER //
CREATE PROCEDURE UpdateMMV(Num VARCHAR(10), NumAnt VARCHAR(10), State VARCHAR(10))
BEGIN
	IF (SELECT COUNT(*) FROM OrdersTest) > 0 then
		UPDATE CustomersTest A INNER JOIN OrdersTest B ON A.CustomerID = B.CustomerID SET
		A.CustomerID = Num,
		A.State = State,
		B.State = state
		WHERE A.CustomerID = NumAnt;
	ELSE
		UPDATE CustomersTest SET CustomerID = Num, State = State WHERE CustomerID = NumAnt;
	END IF;
END //

DELIMITER ;

call UpdateMMV("NUMERO1", "NUMERO3", "ACTIVO");

select	A.CustomerID CustomerIDCustomer,
		A.State CustomerState,
        B.CustomerID CustomerIDOrder,
        B.State OrderState
from CustomersTest A INNER JOIN OrdersTest B ON A.CustomerID = B.CustomerID;

