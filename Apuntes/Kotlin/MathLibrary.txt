	
	//* Elevar un numero a una potencia
	Math.pow(5.0, 3.0) // 125.0

	//* Recibe un valor double y devuelve el valor mas pequeño que es mayor o igual que el valor dado
	var ceil = Math.ceil(3.5)
	println(ceil)

	//* Raiz cuadrada de un numero
	var sqrt = Math.sqrt(25.0)
	println(sqrt)
 	
	//* Valor absoluto de un numero
	var abs = Math.abs(-20)
	println(abs)

	var floor = Math.floor(6.9)
	println(floor)

	//* Devuelve el valor mas pequeño de dos numeros dados, su contraparte es max
	Math.min(5, 3) // 3

	//* Genera un numero random entre 0 y 1
	Math.random() // 0.3335735290527727
	
	//* Redondea el numero double dado a un entero
	Math.round(15.7) // 16

	//* Recibe un double y retorna el valor mas grande que es mejor o igual a este
	var floor = Math.floor(6.9)