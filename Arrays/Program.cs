﻿// Online C# Editor for free
// Write, Edit and Run your C# code using C# Online Compiler

using System;

public class Program
{
    public static void Main(string[] args)
    {
        int[,] array = {{12,0,1,58},{2, 30, 10, 88885}};

        Console.WriteLine("".PadRight(41,'_'));

        Console.WriteLine(
            "| {0, -7} | {1, -7} | {2, -7} | {3, -7} |",
            "Primero",
            "Segundo",
            "Tercero",
            "Cuarto");

        Console.WriteLine("".PadRight(41, '-'));

        for (int i = 0; i < 2; i++){
            for (int j = 0; j < 4; j++)
                Console.Write("| {0, -8}", array[i,j]);

            Console.Write("|\n");
        }

        Console.WriteLine("________________________________________");

    }
}